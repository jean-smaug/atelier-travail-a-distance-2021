# Atelier - Travail à distance

Ce projet utilise [CSS Zen Garden](http://www.csszengarden.com/)

Objectifs :

1. Cloner un projet &rarr; `git clone PROJECT_URL`
1. Créer une branche &rarr; `git branch NOM_DE_BRANCHE` puis `git switch NOM_DE_BRANCHE`
1. Créer un commit &rarr; Modification des fichiers puis `git add .` puis `git commit -m "message de commit"`
1. Créer une Merge Request
1. Mettre à jour sa branche &rarr; `git pull origin BRANCHE_COMMUNE`
1. Résoudre des conflits 

## Règles

- Vous devez travailler sur une branche dédiée `nom1-nom2`. Ex : `clinton-trump`. Cette branche sera "votre **master**". Chaque binome doit créer des branches à partir de "son **master**".

- Avant de commencer à développer, vous devez compléter le fichier `developer.txt`

- En binôme vous allez devoir customiser la page `index.html`

  - Ajouter du HTML
  - Ajouter du CSS (inline, feuille de style externe)

- Interdiction de communiquer via Discord ou équivalent. Vous pouvez communiquer via les Merge Request.

- Vous pouvez vous inspirer de [ces designs](http://www.mezzoblue.com/zengarden/alldesigns/)

- JS autorisé
